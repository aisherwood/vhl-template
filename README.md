# VHL Reporting Template

This document was largely inspired by the whoisflynn template for the OSCP: https://github.com/whoisflynn/OSCP-Exam-Report-Template/

Major changes:
- Replaced Branding with VHL imaging.
- Changed Requirements to reflect VHL reporting needs.
- Changed the structure of the displayed information into a table format.

Feel free to use!

# VHL Tracker

This is an excel document I created to help me track my progress across the various VHL machines. I kept my current progress (at the time of this writing) in the document to give an idea of how I have everything setup. The "Roots" column is definitely outdated at this point, feel free to update these values.

The end results is a list formatted first by difficulty then by amount of roots, which was my way of solving the boxes easiest to hardest.